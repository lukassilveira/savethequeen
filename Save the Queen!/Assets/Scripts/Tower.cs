﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tower : MonoBehaviour
{
    [SerializeField] public Slider health;
    [SerializeField] public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();    
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 14){
            StartCoroutine(FlickerSprite());
            if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.Play();
            health.value -= other.GetComponentInParent<Enemy>().damage;
        }
    }

    IEnumerator FlickerSprite()
    {
        for (int i = 0; i < 2; i++)
        {
            GetComponent<SpriteRenderer>().color = new Color(0.5f, 0, 0, 0.5f);
            yield return new WaitForSeconds(.1f);
            GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            yield return new WaitForSeconds(.1f);
        }
    }
}
