﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSelfDestruct : MonoBehaviour
{
    private ParticleSystem ps;

    void Awake()
    {
        ps = GetComponent<ParticleSystem>();    
    }

    void Update()
    {
        if (ps)
        {
            if (ps.IsAlive())
            {
                Destroy(gameObject, 4f);
            }
        }
    }
}
