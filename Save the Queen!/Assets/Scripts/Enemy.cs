﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemy : Character
{
    [Header("Variables (Enemy)")]
    [SerializeField] private float searchLine;
    [SerializeField] private LayerMask tower;
    private RaycastHit2D searchTowerRight;
    private RaycastHit2D searchTowerLeft;
    [SerializeField] private float attackTimer = 0;
    [SerializeField] Transform deathGameObject;
    [SerializeField] ParticleSystem damageParticles;
    [SerializeField] ParticleSystem deathParticles;
    [SerializeField] private int scoreOnDeath;
    [SerializeField] private GameObject scorePopText;
    [SerializeField] private Animator queen;

    [Header("Sounds")]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip damageClip;
    [SerializeField] private AudioClip slash;

    [Header("Power Up")]
    [SerializeField] private GameObject heart;
    [SerializeField] private GameObject doubleDamage;
    [SerializeField] private GameObject haste;
    [SerializeField] private GameObject doubleScore;

    [Header("Flags (Enemy)")]
    private bool canAttack = false;
    
    void Awake()
    {
        attackDelay = 2;
        FlipOnSpawn();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        hud = GameObject.Find("HUDController").GetComponentInParent<HUDController>();
        queen = GameObject.Find("Queen").GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        //scorePopText.GetComponent<TextMeshPro>().text = "+" + scoreOnDeath.ToString();
    }

    void Update()
    {
        Move(rb, transform.localScale.x, runSpeed, 1);
        RaycastForTower();
        Attack(runSpeed);
        Death();
        AnimatorController();
    }

    void AnimatorController()
    {
        anim.SetFloat("hSpeed", Mathf.Abs(rb.velocity.x));
        //anim.SetBool("IsAttacking", attackArea.active);
    }
    
    void OnHit()
    {
        queen.Play("Damage");
    }

    public void Death()
    {
        if (health <= 0)
        {
            scoreOnDeath *= hud.enemyMult;
            DropItem();
            deathGameObject.parent = null;
            deathParticles.Play();
            hud.scoreNumber += scoreOnDeath;
            Destroy(gameObject);
            ScorePopText();
        }
    }

    void ScorePopText()
    {
        var go = Instantiate(scorePopText, transform.position, transform.rotation);
        go.GetComponentInChildren<TextMeshPro>().text = "+" + scoreOnDeath.ToString();
    }

    void DropItem()
    {
        float chance = Random.Range(0.0f, 1.0f);
        if (chance <= 0.15f)
        {
            Instantiate(heart, new Vector2(transform.position.x, transform.position.y), transform.rotation);
        }

        else if (chance <= 0.18f)
        {
            Instantiate(haste, new Vector2(transform.position.x, transform.position.y), transform.rotation);
        }

        else if (chance <= 0.21f)
        {
            Instantiate(doubleScore, new Vector2(transform.position.x, transform.position.y), transform.rotation);
        }

        else if (chance <= 0.26f)
        {
            Instantiate(doubleDamage, new Vector2(transform.position.x, transform.position.y), transform.rotation);
        }
        
        else if (chance >= 0.27f)
        {
            return;
        }
    }

    void FlipOnSpawn()
    { 
        if(transform.position.x > 0)
        {
            Flip();
        }
    }

    void RaycastForTower()
    {
        searchTowerRight = Physics2D.Raycast(transform.position, Vector2.right, searchLine, tower);
        if (searchTowerRight.collider != null)
        {
            canAttack = true;
        }

        searchTowerLeft = Physics2D.Raycast(transform.position, Vector2.left, searchLine, tower);
        if (searchTowerLeft.collider != null)
        {
            canAttack = true;
        }
    }

    void Attack(float originalRunSpeed)
    {
        if (canAttack)
        {
            runSpeed = 0;
            attackTimer += Time.deltaTime;
            if (attackTimer >= 2.5f)
            {
                anim.SetTrigger("IsAttacking");
                attackTimer = 0;
                //attackArea.SetActive(true);
                /*if (attackTimer >= 2.1f)
                {
                    //attackArea.SetActive(false);
                    anim.SetTrigger("IsAttacking");
                    attackTimer = 0;
                }*/
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 13)
        {
            health -= other.GetComponentInParent<Player>().currentDamage;
            if(health > 0)
            {
                damageParticles.Play();
                StartCoroutine("FlickerSprite");
                if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(damageClip, .75f);
                if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(slash, .15f);
            }
        }
    }
}
