﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform TopLeft;
    public Transform BottomLeft;
    public Transform TopRight;
    public Transform BottomRight;

    [SerializeField] private HUDController hudcontroller;

    private float minSpawnTime = 1;
    private float maxSpawnTime = 3;
    private float spawnTime = 1.5f;


    public GameObject[] monster;

    private void Awake()
    {
        Spawn();   
    }

    private void Update()
    {
        Progression();
    }

    public void Progression()
    {
        if (hudcontroller.scoreNumber <= 5000)
        {
            minSpawnTime = 1;
            maxSpawnTime = 3;

        }

        else if (hudcontroller.scoreNumber <= 10000)
        {
            minSpawnTime = 0.8f;
            maxSpawnTime = 2.4f;
        }

        else if (hudcontroller.scoreNumber <= 15000)
        {
            minSpawnTime = 0.6f;
            maxSpawnTime = 1.7f;
        }

        else if (hudcontroller.scoreNumber <= 20000)
        {
            minSpawnTime = 0.5f;
            maxSpawnTime = 1f;
        }

        else if (hudcontroller.scoreNumber > 20000) 
        {
            minSpawnTime = 0.3f;
            maxSpawnTime = 0.6f;
        }
    }

    public void Spawn()
    {
        int j = 0;
        int i = Random.Range(1, 5);
        float chance = Random.Range(0.0f, 1.0f);

        if (chance <= .07f)
        {
            j = 1;
        }
        else
        {
            j = 0;
        }

        switch (i)
        {
            case 1:
                Instantiate(monster[j], TopLeft.position, transform.rotation);
                break;
            case 2:
                Instantiate(monster[j], BottomLeft.position, transform.rotation);
                break;
            case 3:
                Instantiate(monster[j], TopRight.position, transform.rotation);
                break;
            case 4:
                Instantiate(monster[j], BottomRight.position, transform.rotation);
                break;
        }
        Invoke("Spawn", Random.Range(minSpawnTime, maxSpawnTime));
    }
}
