﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePopText : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip deathClip;
    [SerializeField] private AudioClip slash;
    [SerializeField] public float deathVolume;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        transform.position = transform.position + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(0, 0.45f));
        if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(deathClip, .75f);
        if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(slash, .15f);
        Destroy(gameObject, 3f);
    }
}
