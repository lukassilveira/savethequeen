﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("Flags (Character)")]
    public bool canFlip = true;
    [Header("Variables (Character)")]
    public GameObject attackArea;
    public float health;
    public int damage;
    public float attackDelay;
    public bool facingRight = true;
    public float runSpeed;
    public Rigidbody2D rb;
    public Animator anim;
    public SpriteRenderer sprite;
    public HUDController hud;
    public bool rbHandler = true;
    public Vector3 theScale;

    public void Move(Rigidbody2D rb, float horizontalDirection, float runSpeed, float modifier)
    {
        if (rbHandler)
        {
            rb.velocity = new Vector2(horizontalDirection * runSpeed * modifier, rb.velocity.y);
            if (horizontalDirection > 0 && !facingRight)
            {
                Flip();
            }
            else if (horizontalDirection < 0 && facingRight)
            {
                Flip();
            }
        }
    }

    public void Jumping(Rigidbody2D rb, float jumpForce)
    {
        rb.velocity += Vector2.up * jumpForce;
    }

    public void Attack(GameObject attackArea)
    {
        attackArea.SetActive(true);
    }

    public void Flip()
    {
        if (canFlip)
        {
            facingRight = !facingRight;
            theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }      
    }

    IEnumerator FlickerSprite()
    {
        for (int i = 0; i < 2; i++)
        {
            sprite.color = new Color(1, 0, 0, 1);
            yield return new WaitForSeconds(.1f);
            sprite.color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(.1f);
        }
    }
}
