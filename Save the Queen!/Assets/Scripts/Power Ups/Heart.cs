﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Item
{
    [SerializeField] private int restore;
    [SerializeField] Tower tower;

    void Awake()
    {
        tower = GameObject.Find("Tower").GetComponent<Tower>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        collectCooldown -= Time.deltaTime;
        TimeToPick();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collectCooldown <= 0)
        {
            if (collision.gameObject.layer == 12)
            {
                tower.health.value += restore;
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(pickUpClip, .5f);
                Destroy(gameObject, 1f);
            }
        }
    }
}
