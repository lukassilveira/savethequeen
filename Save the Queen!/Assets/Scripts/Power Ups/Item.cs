﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] public float collectCooldown = .5f;
    [SerializeField] public AudioSource audioSource;
    [SerializeField] public AudioClip pickUpClip;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void TimeToPick()
    {
        if (collectCooldown < -5)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collectCooldown <= 0)
        {
            if (collision.gameObject.layer == 12)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                audioSource.PlayOneShot(pickUpClip);
                Destroy(gameObject, 1f);
            }
        }
    }
}
