﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private PlatformEffector2D platform;
    [SerializeField] private float timer = 0;
    [SerializeField] private Joystick joystick;
    [SerializeField] private float originalOffset;

    void Awake()
    {
        platform = GetComponent<PlatformEffector2D>();
        originalOffset = platform.rotationalOffset;
    }

    void Update()
    {
        //timer -= Time.deltaTime;

        if (joystick.Vertical <= -0.5f)
        {
            platform.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }

        else 
        {
            platform.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

}
