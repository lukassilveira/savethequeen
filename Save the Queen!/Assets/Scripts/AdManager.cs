﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour
{
    private RewardBasedVideoAd rewardBasedVideoAd;
    private string appId = "ca-app-pub-2746515646702609~4518042352";
    private string sampleId = "ca-app-pub-3940256099942544~3347511713";
    private string rewardedVideoId = "ca-app-pub-2746515646702609/4953556975";
    private string sampleRewardedVideoId = "ca-app-pub-3940256099942544/5224354917";

    private void Start()
    {  
        MobileAds.Initialize(appId);
        this.rewardBasedVideoAd = RewardBasedVideoAd.Instance;
    }

    public void RequestRewardedVideo()
    {
        AdRequest adRequest = new AdRequest.Builder().Build();
        rewardBasedVideoAd.LoadAd(adRequest, rewardedVideoId);
    }
}
