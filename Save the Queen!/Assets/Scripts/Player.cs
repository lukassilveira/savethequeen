﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ActionCode2D.Renderers;

public class Player : Character
{
    [Header("Variables (Player)")]
    [SerializeField] private Transform GroundCheck;
    [SerializeField] private float groundedBoxSize = 0.5f;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private float horizontalDirection;
    [SerializeField] private float jumpForce;
    [SerializeField] public int currentDamage;
    [SerializeField] private int doubleDamage;
    [SerializeField] private float currentSpeed;
    [SerializeField] private int hasteSpeed;
    [SerializeField] private float speedModifier;
    [SerializeField] private Joystick joystick;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip attackClip;

    [Header("Particles")]
    [SerializeField] private ParticleSystem runningParticles;
    [SerializeField] private ParticleSystem doubleDamageParticles;
    [SerializeField] private ParticleSystem hasteParticles;
    [SerializeField] private ParticleSystem doubleScoreParticles;
    [SerializeField] private SpriteGhostTrailRenderer ghost;

    [Header("Flags (Player)")]
    [SerializeField] private bool isAttacking = false;
    [SerializeField] private bool canAttack = true;
    [SerializeField] private bool canJump = false;
    [SerializeField] private bool isGrounded;

    [Header("Power Up Timers")]
    [SerializeField] public float doubleDamageTimer = 0;
    [SerializeField] public float hasteTimer = 0;
    [SerializeField] public float doubleScoreTimer = 0;


    [Header("Power Up Sliders")]
    [SerializeField] private Slider doubleDamageSlider;
    [SerializeField] private Slider hasteSlider;
    [SerializeField] private Slider doubleScoreSlider;

    void Awake()
    {
        speedModifier = 1;
        doubleDamage = 6;
        hasteSpeed = 4;
        anim = GetComponent<Animator>();
        attackArea = transform.Find("AttackArea").gameObject;
        rb = GetComponent<Rigidbody2D>();
        runningParticles = transform.Find("RunningParticles").GetComponent<ParticleSystem>();
        doubleDamageParticles = transform.Find("DoubleDamageParticles").GetComponent<ParticleSystem>();
        hasteParticles = transform.Find("HasteParticles").GetComponent<ParticleSystem>();
        doubleScoreParticles = transform.Find("DoubleScoreParticles").GetComponent<ParticleSystem>();
        ghost = GetComponent<SpriteGhostTrailRenderer>();
        sprite = GetComponent<SpriteRenderer>();
        hud = GameObject.Find("HUDController").GetComponent<HUDController>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        AnimatorController();
        //Attack();
        movementDirection();
        //Jump();
        GroundTreatment();
        Move(rb, horizontalDirection, currentSpeed, speedModifier);
        ParticleController();
        PowerUpController();
    }

    //Attacking
    public void Attack()
    {
        if (canAttack)
        {
            anim.Play("Attack");
            if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(attackClip, .5f);
            /*if (Input.GetKeyDown(KeyCode.X))*/ //StartCoroutine("AttackRoutine");
        }
    }

    //Moving
    public void movementDirection(/*int direction*/)
    {
        //horizontalDirection = Input.GetAxis("Horizontal");
        //horizontalDirection = direction;

        if (isAttacking)
        {
            speedModifier = 0.5f;
        }
        else
        {
            speedModifier = 1;
        }

        if (joystick.Horizontal > .15f)
        {
            horizontalDirection = 1;
        }

        else if (joystick.Horizontal < -.15f)
        {
            horizontalDirection = -1;
        }

        else
        {
            horizontalDirection = 0;
        }
    }

    //Jumping
    public void Jump()
    {
        if (/*Input.GetKeyDown(KeyCode.Z) && isGrounded && rb.velocity.y < 0.01f*/ canJump && joystick.Vertical > 0.35f && isGrounded && rb.velocity.y < 0.01f)
        {
            Jumping(rb, jumpForce);
            canJump = false;
        }
    }

    private void GroundTreatment()
    {
        canJump = isGrounded;
        isGrounded = Physics2D.OverlapBox(GroundCheck.position, new Vector2(groundedBoxSize, groundedBoxSize / 2), 0, whatIsGround);
    }

    public void JumpButton()
    {
        if (canJump && isGrounded && rb.velocity.y < 0.01f)
        {
            Jumping(rb, jumpForce);
            canJump = false;
        }
    }


    public IEnumerator AttackRoutine()
    {
        //rbHander = false;
        canFlip = false;
        canAttack = false;
        isAttacking = true;
        //Attack(attackArea);
        yield return new WaitForSeconds(.6f);
        //attackArea.SetActive(false);
        canAttack = true;
        isAttacking = false;
        canFlip = true;
        //rbHander = true;
    }

    void AnimatorController()
    {
        anim.SetBool("isAttacking", isAttacking);
        anim.SetFloat("hSpeed", Mathf.Abs(rb.velocity.x));
        anim.SetFloat("vSpeed", rb.velocity.y);
        anim.SetBool("isGrounded", isGrounded);
    }

    void ParticleController()
    {
        ParticleSystem.EmissionModule em = runningParticles.emission;
        em.enabled = isGrounded;
    }

    void PowerUpController()
    {
        doubleDamageTimer -= Time.deltaTime;
        hasteTimer -= Time.deltaTime;
        doubleScoreTimer -= Time.deltaTime;

        ParticleSystem.EmissionModule ddModule = doubleDamageParticles.emission;
        ParticleSystem.EmissionModule hModule = hasteParticles.emission;
        ParticleSystem.EmissionModule dsModule = doubleScoreParticles.emission;

        //Double Damage
        if (doubleDamageTimer > 0)
        {
            sprite.color = new Color(1, 0.5f, 0.5f);
            ddModule.enabled = true;
            doubleDamageSlider.transform.parent.parent.gameObject.SetActive(true);
            doubleDamageSlider.value = doubleDamageTimer;
            currentDamage = doubleDamage;
        }

        else
        {
            ddModule.enabled = false;
            doubleDamageSlider.transform.parent.parent.gameObject.SetActive(false);
            currentDamage = damage;
            sprite.color = Color.white;
        }

        //Haste
        if (hasteTimer > 0)
        {
            hModule.enabled = true;
            ghost.enabled = true;
            hasteSlider.transform.parent.parent.gameObject.SetActive(true);
            hasteSlider.value = hasteTimer;
            currentSpeed = hasteSpeed;
        }

        else
        {
            ghost.enabled = false;
            hasteSlider.transform.parent.parent.gameObject.SetActive(false);
            currentSpeed = runSpeed;
        }

        //Double Score
        if (doubleScoreTimer > 0)
        {
            dsModule.enabled = true;
            doubleScoreSlider.transform.parent.parent.gameObject.SetActive(true);
            doubleScoreSlider.value = doubleScoreTimer;
            hud.scoreMult = 40;
            hud.enemyMult = 2;
        }

        else
        {
            doubleScoreSlider.transform.parent.parent.gameObject.SetActive(false);
            dsModule.enabled = false;
            hud.scoreMult = 20;
            hud.enemyMult = 1;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(GroundCheck.transform.position, new Vector2(groundedBoxSize, groundedBoxSize/2));
    }
}
