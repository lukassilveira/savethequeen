﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private bool canClick = false;
    [SerializeField] private Animator fadeOutImage;
    [SerializeField] private GameObject highScore;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip clickClip;
    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject tutorialPanel;
    [SerializeField] public int soundOn = 1;
    [Space]
    [SerializeField] private Image[] tutorial;
    [SerializeField] private int tutorialIndex = 0;
    [SerializeField] private Toggle soundToggle;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject backButton;

    private float timer = 0;

    private void Awake()    
    {
        PlayerPrefs.SetInt("soundOn", 1);
        if (PlayerPrefs.GetFloat("Score") != 0)
        {
            highScore.gameObject.SetActive(true);
            highScore.GetComponent<TMP_Text>().text = "High Score: " + PlayerPrefs.GetFloat("Score").ToString("F0");
        }

        soundToggle.isOn = PlayerPrefs.GetInt("soundOn") == 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) PlayerPrefs.SetFloat("Score", 0);
        PlayerPrefs.SetInt("soundOn", soundOn);
        Mathf.Clamp(tutorialIndex, 0, 8);
        timer += Time.deltaTime;
        if (timer > 0) canClick = true;
        nextButton.SetActive(tutorialIndex != 7);
        backButton.SetActive(tutorialIndex != 0);
    }

    public void ClickPlay()
    {
        if (canClick)
        {
            if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(clickClip, 1);
            fadeOutImage.SetTrigger("ClickedPlay");
            Invoke("LoadPlay", 2.5f);
        }
    }

    void LoadPlay()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void ClickHowToPlay()
    {
        if (canClick)
        {
            if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(clickClip, 1);
            tutorialPanel.SetActive(true);
            tutorialIndex = 0;
            mainMenuPanel.SetActive(false);
            tutorial[tutorialIndex].gameObject.SetActive(true);
        }
    }

    public void HowToPlayNext()
    {
        if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(clickClip, 1);
        if (tutorialIndex < 7) tutorialIndex += 1;
        tutorial[tutorialIndex].gameObject.SetActive(true);
        tutorial[tutorialIndex - 1].gameObject.SetActive(false);
    }

    public void HowToPlayBack()
    {
        if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(clickClip, 1);
        if (tutorialIndex > 0) tutorialIndex -= 1;
        tutorial[tutorialIndex + 1].gameObject.SetActive(false);
        tutorial[tutorialIndex].gameObject.SetActive(true);
    }

    public void HowToPlayReturn()
    {
        if (PlayerPrefs.GetInt("soundOn") == 1) audioSource.PlayOneShot(clickClip, 1);
        tutorial[tutorialIndex].gameObject.SetActive(false);
        tutorialIndex = 0;
        tutorialPanel.SetActive(false);
        mainMenuPanel.SetActive(true); 
    }

    public void ToggleSound()
    {
        if (soundOn == 1)
        {
            soundOn = 0;
            audioSource.Stop();
        }

        else
        {
            soundOn = 1;
            audioSource.Play();
        }
    }
}
