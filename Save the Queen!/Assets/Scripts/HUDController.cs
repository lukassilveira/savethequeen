﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class HUDController : MonoBehaviour
{
    [SerializeField] private Tower tower;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private float targetScore;
    [SerializeField] public float scoreNumber;
    [SerializeField] public float scoreMult;
    [SerializeField] public int enemyMult;
    [SerializeField] private bool canNeverGiveUp = true;
    [SerializeField] private Animator fadeOutImage;
    [SerializeField] private Animator fadeInImage;
    [SerializeField] private GameObject[] activateOnGameStart;
    [Space]
    [SerializeField] private GameObject[] deactivateOnGameOver;
    [Space]
    [SerializeField] private GameObject[] activateOnGameOver;
    [Space]
    [SerializeField] private GameObject[] pauseGame;
    [Space]
    [SerializeField] private GameObject[] returnMenu;
    [Space]
    [SerializeField] private GameObject gameOverCanvas;
    [Space]
    [SerializeField] private GameObject neverGiveUpButton;
    [Space]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip inGameSecondClip;
    [SerializeField] private AudioSource gameOverAudioSource;
    [HideInInspector] Animator anim;

    void Awake()
    {
        if (PlayerPrefs.GetInt("soundOn") == 1)
        {
            gameOverAudioSource.playOnAwake = true;
            audioSource.Play();
        }
        StartCoroutine("StartGame");
        scoreNumber = 0;
        scoreMult = 20;
        anim = GetComponent<Animator>();
        enemyMult = 1;
        Screen.SetResolution(1920, 1080, false);
    }

    void Update()
    {
        Score();
        GameOver();
        KillTower();
        CalculateNeverGiveUp();

        if (Input.GetKeyDown(KeyCode.F)) scoreNumber += 5000;
    }
    
    private void CalculateNeverGiveUp()
    {       
        if (scoreNumber >= targetScore)
        {
            canNeverGiveUp = true;
            targetScore += 15000;
        }
    }

    private void RegenerateTower()
    {
        var tower = FindObjectOfType<Tower>();
        tower.health.value = 25;
    }

    private void KillTower()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            tower.health.value = 0;
        }
    }

    private void KillEnemies()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].health = 0;
        }
    }

    private void Score()
    {
        scoreNumber += Time.deltaTime * scoreMult;
        scoreText.text = "Score: " + scoreNumber.ToString("F0");
        if (!audioSource.isPlaying && PlayerPrefs.GetInt("soundOn") == 1)
        {
            audioSource.PlayOneShot(inGameSecondClip, 1);
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        audioSource.volume = .15f;
        for (int i = 0; i < pauseGame.Length; i++)
        {
            pauseGame[i].SetActive(true);
        }

        for (int i = 0; i < returnMenu.Length; i++)
        {
            returnMenu[i].SetActive(false);
        }
    }

    public void Resume()
    {
        audioSource.volume = 1;
        Time.timeScale = 1f;
        for (int i = 0; i < pauseGame.Length; i++)
        {
            pauseGame[i].SetActive(false);
        }
    }

    public void ReturnToMenu()
    {
        for (int i = 1; i < pauseGame.Length; i++)
        {
            pauseGame[i].SetActive(false);
        }
        for (int i = 0; i < returnMenu.Length; i++)
        {
            returnMenu[i].SetActive(true);
        }
    }

    public void Quit()
    {
        StartCoroutine(QuitRoutine());
    }
    
    public void GameOver()
    {
        if (tower.health.value <= 0)
        {
            Time.timeScale = 0f;

            if (PlayerPrefs.GetFloat("Score") < scoreNumber)
            {
                PlayerPrefs.SetFloat("Score", scoreNumber);
            }

            for (int i = 0; i < deactivateOnGameOver.Length; i++)
            {
                deactivateOnGameOver[i].SetActive(false);
            }

            
            for (int i = 0; i < activateOnGameOver.Length; i++)
            {
                activateOnGameOver[i].SetActive(true);
            }

            if (canNeverGiveUp) neverGiveUpButton.SetActive(true);
            
            GameObject.Find("Game Over Score").GetComponent<TMP_Text>().text = "Your score: " + scoreNumber.ToString("F0");
            audioSource.Stop();
        }
    }

    public void TryAgain()
    {
        StartCoroutine("TryAgainRoutine");
        //TryAgainRoutine();
    }

    public void NeverGiveUpRoutine()
    {
        StartCoroutine(NeverGiveUp());
    }

    public IEnumerator NeverGiveUp()
    {
        KillEnemies();
        RegenerateTower();
        Time.timeScale = 1;
        var player = FindObjectOfType<Player>();
        player.hasteTimer = 10;
        player.doubleDamageTimer = 10;
        player.doubleScoreTimer = 10;

        for (int i = 0; i < activateOnGameOver.Length; i++)
        {
            activateOnGameOver[i].GetComponent<Animator>().SetTrigger("NeverGiveUp");
        }

        neverGiveUpButton.GetComponent<Animator>().SetTrigger("NeverGiveUp");

        for (int i = 0; i < deactivateOnGameOver.Length; i++)
        {
            deactivateOnGameOver[i].SetActive(true);
        }

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < activateOnGameOver.Length; i++)
        {
            activateOnGameOver[i].SetActive(false);
        }
        canNeverGiveUp = false;
        neverGiveUpButton.SetActive(false);
    }

    public IEnumerator TryAgainRoutine()
    {
        fadeOutImage.SetTrigger("ClickedTryAgain");
        yield return new WaitForSecondsRealtime(2.5f);
        Time.timeScale = 1f;
        SceneManager.LoadScene("SampleScene");
    }

    public IEnumerator QuitRoutine()
    {
        fadeOutImage.gameObject.SetActive(true);
        fadeOutImage.SetTrigger("ClickedTryAgain");
        yield return new WaitForSecondsRealtime(2.5f);
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public IEnumerator StartGame()
    {
        fadeInImage.SetTrigger("StartedGame");
        yield return new WaitForSeconds(2.5f);
        for (int i = 0; i < activateOnGameStart.Length - 1; i++)
        {
            activateOnGameStart[i].SetActive(true);
        }
        scoreNumber = 0;
        yield return new WaitForSeconds(2.5f);
        activateOnGameStart[activateOnGameStart.Length - 1].SetActive(true);
    }
}
