﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour, IUnityAdsListener
{
    string appId = "3302040";
    string rewardedAd = "rewardedVideo";

    [SerializeField] Button button;
    [SerializeField] HUDController hud;

    void Start()
    {
        button.interactable = Advertisement.IsReady(rewardedAd);
        if (button) button.onClick.AddListener(ShowRewardedVideo);
        Advertisement.AddListener(this);
        Advertisement.Initialize(appId);
    }

    void ShowRewardedVideo()
    {
        Advertisement.Show(rewardedAd);
    }

    void IUnityAdsListener.OnUnityAdsReady(string placementId)
    {
        if (placementId == rewardedAd)
        {
            button.interactable = true;
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.Log(message);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.LogWarning("Ad started :)");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            hud.NeverGiveUpRoutine();
        }

        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error, sorry :(");
        }

        else if (showResult == ShowResult.Skipped)
        {
            hud.TryAgainRoutine();
        }
    }
}
